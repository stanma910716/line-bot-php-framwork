<?php
function sendDatePick($post_data,$start_date,$start_time,$end_date,$end_time){
$n = 0;
foreach ($post_data["messages"] as $key=>$value){
    $n += 1;
}
if($n > 4){
    die("messages array number is max(5).");
}else{
    $today = date("Y-m-d")."t".date("H:m");
    $data = array (
      'type' => 'datetimepicker',
      'label' => 'Select date',
      'data' => 'storeId=12345',
      'mode' => 'date',
      'initial' => $today,
      'max' => $end_date.'t'.$end_time,
      'min' => $start_date.'t'.$start_time,
    );
    $post_data["messages"][$n] = $data;
    return $post_data;
}
}
