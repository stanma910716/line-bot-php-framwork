<?php
function push_message($access_token,$userId,$post_data){
    $push_message = curl_init();
    curl_setopt_array($push_message, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/message/push",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\n    \"to\": \"$userId\",\n    \"messages\":$post_data\n}",
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer NIjVSFrnvP1gDrRVCvV4pidhI/YYDvVax2nxjt44n9kpmeGEpCFNBVNojbGoRvAWVytiR4RsWK/A2XKRaGQgiMZoJaycU1PHOR8/XnEC/8Y+EUWxC1sljNpEe4nMjQCnbsvX90tRiLl6GEhfkqlLBgdB04t89/1O/w1cDnyilFU=",
        "cache-control: no-cache"
    ),
    ));

    $push_response = curl_exec($push_message);
    $push_error = curl_error($push_message);

    curl_close($push_message);

    if ($push_error) {
    echo "cURL Error #:" . $push_error;
    } else {
    echo $push_response;
    }
}

function new_mutlicase_user($user_data){
    $user_data = array();
    return $user_data;
}

function mutlicase_user_list($user_data,$userId){
    $user_data[]=$userId;
    return $user_data;
}

function push_multicast_messages($access_token,$user_data,$post_data){
    $push_message = curl_init();
    curl_setopt_array($push_message, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/message/push",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\n    \"to\": $user_data,\n    \"messages\":$post_data\n}",
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer NIjVSFrnvP1gDrRVCvV4pidhI/YYDvVax2nxjt44n9kpmeGEpCFNBVNojbGoRvAWVytiR4RsWK/A2XKRaGQgiMZoJaycU1PHOR8/XnEC/8Y+EUWxC1sljNpEe4nMjQCnbsvX90tRiLl6GEhfkqlLBgdB04t89/1O/w1cDnyilFU=",
        "cache-control: no-cache"
    ),
    ));

    $push_response = curl_exec($push_message);
    $push_error = curl_error($push_message);

    curl_close($push_message);

    if ($push_error) {
    echo "cURL Error #:" . $push_error;
    } else {
    echo $push_response;
    }
}
