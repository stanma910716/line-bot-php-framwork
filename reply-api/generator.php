<?php
function generator($userId,$event_id){
$g = curl_init();

curl_setopt_array($g, array(
  CURLOPT_URL => "https://api.line.me/liff/v1/apps",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\r\n  \"view\":{\r\n    \"type\":\"full\",\r\n    \"url\":\"https://line.ez2.top/liff/index.php?userId=$userId&event=$event_id\"\r\n  }\r\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer NIjVSFrnvP1gDrRVCvV4pidhI/YYDvVax2nxjt44n9kpmeGEpCFNBVNojbGoRvAWVytiR4RsWK/A2XKRaGQgiMZoJaycU1PHOR8/XnEC/8Y+EUWxC1sljNpEe4nMjQCnbsvX90tRiLl6GEhfkqlLBgdB04t89/1O/w1cDnyilFU=",
    "Cache-Control: no-cache",
    "Content-Type: application/json",
    "Postman-Token: fc7744ba-d203-49de-8acb-bc8688553ab1"
  ),
));

$response = curl_exec($g);
$err = curl_error($g);

curl_close($g);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
    $respon = json_decode($response,true);
    $liff_id = $respon["liffId"];
    return "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chld=L&chl=line://app/".$liff_id;
}
}
