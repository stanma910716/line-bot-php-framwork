<?php
function new_button_template(){
    $new_button_template = [
        "type" => "template",
        "altText" => "test",
        "template" => [
            "type" => "buttons",
            "thumbnailImageUrl" => "https://example.com/thumbnailImageUrl.png",
            "imageAspectRatio" => "rectangle",
            "imageSize" => "cover",
            "imageBackgroundColor" => "#FFFFFF",
            "title" => "button template",
            "text" => "This is a example for Button template",
            "actions" => [
            ]
        ]
    ];
    return $new_button_template;
}


function setup_button_template($button_template,$altText,$thumbnailImageUrl,$imageAspectRatio,$imageSize,$imageBackgroundColor,$title,$text){
    if($imageAspectRatio != "rectangle" and $imageAspectRatio != "square"){
        die("variable error:imageAspectRatio is not valid.");
    }
    if($imageSize != "cover" and $imageSize != "contain"){
        die("variable error:imageSize is not valid.");
    }
    $button_template["altText"] = $altText;
    $button_template["template"]["thumbnailImageUrl"] = $thumbnailImageUrl;
    $button_template["template"]["imageAspectRatio"] = $imageAspectRatio;
    $button_template["template"]["imageSize"] = $imageSize;
    $button_template["template"]["imageBackgroundColor"] = $imageBackgroundColor;
    $button_template["template"]["title"] = $title;
    $button_template["template"]["text"] = $text;
    return $button_template;
}

function set_default_uri_button_template($button_template,$label,$uri){
    $button_template["template"]["defaultAction"] = [
        "type" => "uri",
        "label" =>  "$label",
        "uri" =>  "$uri"
    ];
    return $button_template;
}

function set_default_postback_button_template($button_template,$label,$data){
    $button_template["template"]["defaultAction"] = [
        "type" => "postback",
        "label" =>  "$label",
        "data" =>  "$data"
    ];
    return $button_template;
}

function set_default_message_button_template($button_template,$label,$text){
    $button_template["template"]["defaultAction"] = [
        "type" => "message",
        "label" =>  "$label",
        "text" =>  "$text"
    ];
    return $button_template;
}

function set_default_datepicker_button_template($button_template,$label,$data,$start_date,$end_date){
    $today = date("Y-m-d");
    $button_template["template"]["defaultAction"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "date",
        'initial' => $today,
        'max' => $end_date,
        'min' => $start_date,
    ];
    return $button_template;
}

function set_default_timepicker_button_template($button_template,$label,$data,$start_time,$end_time){
    $today = date("H:i");
    $button_template["template"]["defaultAction"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "time",
        'initial' => $today,
        'max' => $end_time,
        'min' => $start_time,
    ];
    return $button_template;
}

function set_default_datetimepicker_button_template($button_template,$label,$data,$start_date,$start_time,$end_date,$end_time){
    $today = date("Y-m-d")."t".date("H:i");
    $button_template["template"]["defaultAction"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "datetime",
        'initial' => $today,
        'max' => $end_date.'t'.$end_time,
        'min' => $start_date.'t'.$start_time,
    ];
    return $button_template;
}


function push_message_button_template($button_template,$label,$text){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $button_template["template"]["actions"]["$n"] = [
        "type" => "message",
        "label" =>  "$label",
        "text" =>  "$text"
    ];
    return $button_template;
}

function push_datepicker_button_template($button_template,$label,$data,$start_date,$end_date){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $today = date("Y-m-d");
    $button_template["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "date",
        'initial' => $today,
        'max' => $end_date,
        'min' => $start_date,
    ];
    return $button_template;
}

function push_timepicker_button_template($button_template,$label,$data,$start_time,$end_time){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $today = date("H:i");
    $button_template["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "time",
        'initial' => $today,
        'max' => $end_time,
        'min' => $start_time,
    ];
    return $button_template;
}

function push_datetimepicker_button_template($button_template,$label,$data,$start_date,$start_time,$end_date,$end_time){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $today = date("Y-m-d")."t".date("H:i");
    $button_template["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "datetime",
        'initial' => $today,
        'max' => $end_date.'t'.$end_time,
        'min' => $start_date.'t'.$start_time,
    ];
    return $button_template;
}

function push_postback_button_template($button_template,$label,$data){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $button_template["template"]["actions"]["$n"] = [
        "type" => "postback",
        "label" =>  "$label",
        "data" =>  "$data"
    ];
    return $button_template;
}

function push_uri_button_template($button_template,$label,$uri){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 3){
        die("button template actions array is max(4).");
    }
    $button_template["template"]["actions"]["$n"] = [
        "type" => "uri",
        "label" =>  "$label",
        "uri" =>  "$uri"
    ];
    return $button_template;
}

function send_button_template($post_data,$button_template){
    $n = 0;
    foreach ($post_data["messages"] as $key => $value){
        $n += 1;
    }
    if($n > 4){
        die("messages array number is max(5).");
    }else{
        $post_data["messages"]["$n"] = $button_template;
        return $post_data;
    }
}