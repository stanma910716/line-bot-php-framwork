<?php
function new_confirm_template(){
    $new_confirm_template = [
        "type" => "template",
        "altText" => "test",
        "template" => [
            "type" => "confirm",
            "text" => "Are You Sure?",
            "actions" => [
            ]
        ]
    ];
    return $new_confirm_template;
}

function setup_confirm_template($confirm_data,$altText,$text){
    $confirm_data["altText"] = $altText;
    $confirm_data["template"]["text"] = $text;
    return $confirm_data;
}

function push_message_confirm_template($confirm_data,$label,$text){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm Template is max(2).");
    }
    $confirm_data["template"]["actions"][$n] = [
        "type" => "message",
        "label" => "$label",
        "text" => "$text"
    ];
    return $confirm_data;
}

function push_postback_confirm_template($confirm_data,$label,$data,$text){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm Template is max(2).");
    }
    if(isset($text)){
        $confirm_data["template"]["actions"][$n] = [
            "type" => "postback",
            "label" => "$label",
            "data" => "$data",
            "text" => "$text"
        ];
    }else{
        $confirm_data["template"]["actions"][$n] = [
            "type" => "postback",
            "label" => "$label",
            "data" => "$data",
        ];
    }
}

function push_uri_confirm_template($confirm_data,$label,$uri){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm Template is max(2).");
    }
    $confirm_data["template"]["actions"][$n] = [
        "type" => "uri",
        "label" => "$label",
        "uri" => "$uri"
    ];
    return $confirm_data;
}

function push_datepicker_confirm_template($confirm_data,$label,$data,$start_date,$end_date){
    $n = 0;
    foreach($button_template["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm Template is max(2).");
    }
    $today = date("Y-m-d");
    $confirm_data["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "date",
        'initial' => $today,
        'max' => $end_date,
        'min' => $start_date,
    ];
    return $confirm_data;
}

function push_timepicker_confirm_template($confirm_data,$label,$data,$start_time,$end_time){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm Template is max(2).");
    }
    $today = date("H:i");
    $confirm_data["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "time",
        'initial' => $today,
        'max' => $end_time,
        'min' => $start_time,
    ];
    return $confirm_data;
}

function push_datetimepicker_confirm_template($confirm_data,$label,$data,$start_date,$start_time,$end_date,$end_time){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key=>$value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm template is max(2).");
    }
    $today = date("Y-m-d")."t".date("H:i");
    $confirm_data["template"]["actions"]["$n"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "datetime",
        'initial' => $today,
        'max' => $end_date.'t'.$end_time,
        'min' => $start_date.'t'.$start_time,
    ];
    return $confirm_data;
}

function push_camera_confirm_template($confirm_data,$label){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key => $value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm template is max(2).");
    }
    $confirm_data["template"]["actions"][$n] = [
        "type" => "camera",
        "label" => "$label"
    ];
    return $confirm_data;
}

function push_location_confirm_template($confirm_data,$label){
    $n = 0;
    foreach($confirm_data["template"]["actions"] as $key => $value){
        $n += 1;
    }
    if($n >= 2){
        die("Confirm template is max(2).");
    }
    $confirm_data["template"]["actions"][$n] = [
        "type" => "location",
        "label" => "$label"
    ];
    return $confirm_data;
}

function send_confirm_template($post_data,$confirm_data){
    $n = 0;
    foreach ($post_data["messages"] as $key => $value){
        $n += 1;
    }
    if($n > 4){
        die("messages array number is max(5).");
    }else{
        $post_data["messages"]["$n"] = $confirm_data;
        return $post_data;
    }
}