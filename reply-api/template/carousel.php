<?php
require("dateng_mysql.php");
function new_carousel($carousel){
    $new_carousel = [
          'type' => 'template',
          'altText' => '最近活動',
          'template' =>
          [
            'type' => 'carousel',
            'columns' => [
            ],
            'imageAspectRatio' => 'rectangle',
            'imageSize' => 'cover',
          ],
        ];
        return $new_carousel;
}
function push_carousel ($carousel_data,$event_id,$event_pict,$event_name,$event_place,$event_date,$event_time){
      $n = 0;
      foreach ($carousel_data["template"]["columns"] as $key=>$value){
        $n += 1;
      }
      if($n > 9){
        die("quick reply array number is max(10).");
      }else{
        $carousel_data["template"]["columns"]["$n"] = [
        'thumbnailImageUrl' => $event_pict,
        'imageBackgroundColor' => '#000000',
        'title' => $event_name,
        'text' => "地點：".$event_place."\n"."日期：".$event_date."\n"."時間：".$event_time,
        'defaultAction' =>
        [
            'type' => 'postback',
            'label' => '報名',
            'data' => 'signup='.$event_id,
        ],
        'actions' =>
        [
          0 =>
          [
            'type' => 'postback',
            'label' => '報名',
            'data' => 'signup='.$event_id,
          ]
        ],
      ];
        return $carousel_data;
      }
}
function send_carousel($post_data,$carousel){
    $n = 0;
    foreach ($post_data["messages"] as $key => $value){
        $n += 1;
    }
    if($n > 4){
        die("messages array number is max(5).");
    }else{
        $post_data["messages"]["$n"] = $carousel;
        return $post_data;
    }
}

