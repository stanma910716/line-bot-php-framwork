<?php
function new_rich_menu(){
    $rich_menu_data = [
        "size" => [
            "width" => 2500,
            "height" => 1686
        ],
        "selected" => false,
        "name" => "Rich Menu",
        "chatBarText" => "Rich Menu",
        "areas" =>[
        ]
    ];
    return $rich_menu_data;
}

function setup_rich_menu_size($rich_menu_data,$width,$height){
    $rich_menu_data["size"]["width"] = $width;
    $rich_menu_data["size"]["height"] = $height;
    return $rich_menu_data;
}

function setup_rich_menu_detail($rich_menu_data,$name,$chatBarText){
    $rich_menu_data["name"] = $name;
    $rich_menu_data["chatBarText"] = $chatBarText;
    return $rich_menu_data;
}

function push_message_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$text){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"]["type"] = "message";
    $rich_menu_data["areas"][$n]["action"]["label"] = $label;
    $rich_menu_data["areas"][$n]["action"]["text"] = $text;
    return $rich_menu_data;
}

function push_postback_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$text,$data){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"]["type"] = "postback";
    if(isset($label)){
        $rich_menu_data["areas"][$n]["action"]["label"] = $label;
    }
    $rich_menu_data["areas"][$n]["action"]["data"] = $data;
    if(isset($text)){
        $rich_menu_data["areas"][$n]["action"]["text"] = $text;
    }

    return $rich_menu_data;
}

function push_uri_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$uri){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"]["type"] = "uri";
    $rich_menu_data["areas"][$n]["action"]["label"] = $label;
    $rich_menu_data["areas"][$n]["action"]["uri"] = $uri;
    return $rich_menu_data;
}

function push_datepicker_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$data,$start_date,$end_date){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $today = date("Y-m-d");
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "date",
        'initial' => $today,
        'max' => $end_date,
        'min' => $start_date,
    ];
    return $rich_menu_data;
}

function push_timepicker_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$data,$start_time,$end_time){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $today = date("H:i");
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "time",
        'initial' => $today,
        'max' => $end_time,
        'min' => $start_time,
    ];
    return $rich_menu_data;
}

function push_datetimepicker_rich_menu($rich_menu_data,$x,$y,$width,$height,$label,$data,$start_date,$start_time,$end_date,$end_time){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $today = date("Y-m-d")."t".date("H:i");
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["actions"] = [
        "type" => "datetimepicker",
        "label" =>  "$label",
        "data" =>  "$data",
        "mode" => "datetime",
        'initial' => $today,
        'max' => $end_date.'t'.$end_time,
        'min' => $start_date.'t'.$start_time,
    ];
    return $rich_menu_data;
}

function push_camera_rich_menu($rich_menu_data,$x,$y,$width,$height,$label){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"]["type"] = "camera";
    $rich_menu_data["areas"][$n]["action"]["label"] = $label;
    return $rich_menu_data;
}

function push_location_rich_menu($rich_menu_data,$x,$y,$width,$height,$label){
    $n = 0;
    foreach($rich_menu_data["areas"] as $key=>$value){
        $n += 1;
    }
    if($n >= 20){
        die("Rich Menu Areas is max(20).");
    }
    $rich_menu_data["areas"][$n]["bounds"]["x"] = $x;
    $rich_menu_data["areas"][$n]["bounds"]["y"] = $y;
    $rich_menu_data["areas"][$n]["bounds"]["width"] = $width;
    $rich_menu_data["areas"][$n]["bounds"]["height"] = $height;
    $rich_menu_data["areas"][$n]["action"]["type"] = "location";
    $rich_menu_data["areas"][$n]["action"]["label"] = $label;
    return $rich_menu_data;
}

function send_rich_menu($access_token,$rich_menu_data){
    $data = json_encode($rich_menu_data);
    $send_rich_menu_curl = curl_init();

    curl_setopt_array($send_rich_menu_curl, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/richmenu",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $data,
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer ".$access_token,
        "Content-Type: application/json",
        "cache-control: no-cache"
    ),
    ));

    $rich_menu_response = curl_exec($send_rich_menu_curl);
    $rich_menu_err = curl_error($send_rich_menu_curl);

    curl_close($send_rich_menu_curl);

    if ($rich_menu_err) {
    return "cURL Error #:" . $rich_menu_err;
    } else {
    $rich_menu_return_data = json_decode($rich_menu_response,true);
    return $rich_menu_return_data["richMenuId"];
    }
}

function setup_rich_menu_image($access_token,$rich_menu_id,$file_location){
    $send_rich_menu_image_curl = curl_init();

    curl_setopt_array($send_rich_menu_image_curl, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/richmenu/$rich_menu_id/content",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_POSTFIELDS => file_get_contents($file_location),
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer $access_token",
        "Content-Type: image/png",
        "cache-control: no-cache"
    ),
    ));

    $rich_menu_image_response = curl_exec($send_rich_menu_image_curl);
    $rich_menu_image_err = curl_error($send_rich_menu_image_curl);

    curl_close($send_rich_menu_image_curl);

    if ($rich_menu_image_err) {
        return "cURL Error #:" . $rich_menu_image_err;
    } else {
        return $rich_menu_image_response;
    }
}

function set_default_rich_menu_everyone($access_token,$rich_menu_id){
    $default_rich_menu_curl = curl_init();

    curl_setopt_array($default_rich_menu_curl, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/user/all/richmenu/".$rich_menu_id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer $access_token",
        "cache-control: no-cache"
    ),
    ));

    $default_rich_menu_response = curl_exec($default_rich_menu_curl);
    $default_rich_menu_err = curl_error($default_rich_menu_curl);

    curl_close($default_rich_menu_curl);

    if ($default_rich_menu_err) {
    return "cURL Error #:" . $default_rich_menu_err;
    } else {
    return $default_rich_menu_response;
    }
}

function set_default_rich_menu_to_user($access_token,$rich_menu_id,$userId){
    $default_rich_menu_curl = curl_init();

    curl_setopt_array($default_rich_menu_curl, array(
    CURLOPT_URL => "https://api.line.me/v2/bot/user/".$userId."/richmenu/".$rich_menu_id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer $access_token",
        "cache-control: no-cache"
    ),
    ));

    $default_rich_menu_response = curl_exec($default_rich_menu_curl);
    $default_rich_menu_err = curl_error($default_rich_menu_curl);

    curl_close($default_rich_menu_curl);

    if ($default_rich_menu_err) {
    return "cURL Error #:" . $default_rich_menu_err;
    } else {
    return $default_rich_menu_response;
    }
}