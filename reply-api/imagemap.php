<?php
function new_imagemap($baseurl,$altText){
    $new_imagemap = [
        "type" => "imagemap",
        "baseurl" => $baseurl,
        "altText" => $altText,
        "basesize" => [
            "height" => 1040,
            "width" => 1040
        ],
        "actions" => [
        ]
    ];
    return $new_imagemap;
}

function setup_imagemap($imagemap,$base_url,$altText){
    $imagemap["baseUrl"] = $base_url;
    $imagemap["altText"] = $altText;
    return $imagemap;
}

function push_uri_imagemap($imagemap,$link,$x,$y,$width,$height){
    $n = 0;
    foreach($imagemap["actions"] as $key=>$value){
        $n += 1;
    }
    if($n > 49){
        die("imagemap array number is max(50).");
    }
    $imagemap["actions"]["$n"] =  [
        "type" => "uri",
        "linkUri" => "$link",
        "area" =>[
            "x" => $x,
            "y" => $y,
            "width" => $width,
            "height" => $height
        ]
    ];
    return $imagemap;
}

function send_imagemap($post_data,$imagemap){
    $n = 0;
    $n = 0;
    foreach ($post_data["messages"] as $key => $value){
        $n += 1;
    }
    if($n > 4){
        die("messages array number is max(5).");
    }else{
        $post_data["messages"]["$n"] = $imagemap;
        return $post_data;
    }
}