<?php
header("content-type:application/json");
$json = [
    "size" => [
        "width" => 2500,
        "height" => 1686
    ],
    "selected" => false,
    "name" => "Nice RichMenu",
    "chatBarText" => "Tap to open",
    "areas" => [
        "bounds" => [
            "x" => 0,
            "y" => 0,
            "width" => 2500,
            "height" => 1686
        ],
        "actions" => [
            "type" => "message",
            "text" => "報名資訊",
            "label" => "報名資訊"
        ]
    ]
];
print_r(json_encode($json));