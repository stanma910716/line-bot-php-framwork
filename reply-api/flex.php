<?php
function sendFlex($post_data,$event_title,$event_date,$event_time,$event_place,$qr,$price){
$n = 0;
foreach ($post_data["messages"] as $key=>$value){
    $n += 1;
}
if($n > 4){
    die("messages array number is max(5).");
}else{
    $data = array (
  'type' => 'flex',
  'altText' => '報名資訊',
  'contents' =>
  array (
    'type' => 'bubble',
    'body' =>
    array (
      'type' => 'box',
      'layout' => 'vertical',
      'spacing' => 'md',
      'contents' =>
      array (
        0 =>
        array (
          'type' => 'text',
          'text' => $event_title,
          'wrap' => true,
          'weight' => 'bold',
          'gravity' => 'center',
          'size' => 'xl',
        ),
        1 =>
        array (
          'type' => 'box',
          'layout' => 'vertical',
          'margin' => 'lg',
          'spacing' => 'sm',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'spacing' => 'sm',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'text',
                  'text' => '時間',
                  'color' => '#aaaaaa',
                  'size' => 'sm',
                  'flex' => 1,
                ),
                1 =>
                array (
                  'type' => 'text',
                  'text' => $event_date." ".$event_time,
                  'wrap' => true,
                  'size' => 'sm',
                  'color' => '#666666',
                  'flex' => 4,
                ),
              ),
            ),
            1 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'spacing' => 'sm',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'text',
                  'text' => '地點',
                  'color' => '#aaaaaa',
                  'size' => 'sm',
                  'flex' => 1,
                ),
                1 =>
                array (
                  'type' => 'text',
                  'text' => $event_place,
                  'wrap' => true,
                  'color' => '#666666',
                  'size' => 'sm',
                  'flex' => 4,
                ),
              ),
            ),
            2 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'spacing' => 'sm',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'text',
                  'text' => '報名費',
                  'color' => '#aaaaaa',
                  'size' => 'sm',
                  'flex' => 1,
                ),
                1 =>
                array (
                  'type' => 'text',
                  'text' => $price.' 元',
                  'wrap' => true,
                  'color' => '#666666',
                  'size' => 'sm',
                  'flex' => 4,
                ),
              ),
            ),
        ),
        ),
        2 =>
        array (
          'type' => 'box',
          'layout' => 'vertical',
          'margin' => 'xxl',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'spacer',
            ),
            1 =>
            array (
              'type' => 'image',
              'url' => $qr,
              'aspectMode' => 'cover',
              'size' => 'xl',
            ),
            2 =>
            array (
              'type' => 'text',
              'text' => 'QRcode為本活動入場憑證，請截圖或列印出來提供工作人員驗票使用。',
              'color' => '#aaaaaa',
              'wrap' => true,
              'margin' => 'xxl',
              'size' => 'xs',
            ),
          ),
        ),
    ),
  ),
 ),
);
    $post_data["messages"][$n] = $data;
    return $post_data;
}
}
