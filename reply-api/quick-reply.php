<?php
function new_quick_reply(){
    $new_quick_reply = [
        "type"=>  "text",
        "text"=>  "test",
        "quickReply"=>  [
        "items"=>  [
        ]
      ]
    ];
    return $new_quick_reply;
}

function setup_quick_reply($quick_reply,$label){
    $quick_reply["text"] = $label;
    return $quick_reply;
}

function push_message_quick_reply($quick_reply,$pitcurl,$label,$text){
    $n = 0;
    foreach ($quick_reply["quickReply"]["items"] as $key=>$value){
        $n += 1;
    }
    if($n > 12){
        die("quick reply array number is max(13).");
    }else{
    $quick_reply["quickReply"]["items"]["$n"] = [
        "type" =>  "action",
        "imageUrl" =>  "$pitcurl",
        "action" =>  [
          "type" =>  "message",
          "label" =>  "$label",
          "text" =>  "$text"
        ]
    ];
    return $quick_reply;
}
}

function send_quick_reply($post_data,$quick_reply){
    $n = 0;
    foreach ($post_data["messages"] as $key => $value){
        $n += 1;
    }
    if($n > 4){
        die("messages array number is max(5).");
    }else{
        $post_data["messages"]["$n"] = $quick_reply;
        return $post_data;
    }
}